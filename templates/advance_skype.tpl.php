<?php
/**
 * @file
 * Template file for skype status
 *
 * Available variables:
 * $variables: rendered skype ids
 *
 */
?>
<div class="skype-ids">
  <div class="skype-header">
    <span class="skype-ids-title">
      <img src="<?php print $GLOBALS['base_url']; ?>/sites/all/modules/advance_skype/img/image_skype.png">
    </span>
    <span id="title"><?php print t('Check Who\'s Online?'); ?></span>
  </div>
  <div class="block-skype-ids">
    <ul class="skype-ids-lists">
      <?php if (!empty($variables['skype_ids'])) {
        foreach ($variables['skype_ids'] as $variable): ?>
      <div class="advance-skype-details">
        <li class="advance-skype-alias">
          <span class="skype-ids-image">
            <a href="skype:<?php print $variable['skype_id'] . '?' . $variable['action']; ?>"><img src="<?php print $variable['skype_image_url'];?>"></a>
          </span>
          <span class="skype-ids-alias">
            <?php echo $variable['skype_alias'];?>
          </span>
        </li>
      </div>
      <?php endforeach; } ?>
    </ul>
  </div>
  <div>
    <p class="config-skype">
      <?php print l(t('Configure Skype Ids'), '/user/config/advance_skype', array('attributes' => array('class' => 'edit-skype-ids'))); ?>
    </p>
  </div>
</div>
