<?php
/**
 * @file
 * Configuration setting forms and related functions.
 */

/**
 * Function for user configurations.
 */
function advance_skype_configure_skype_id($form, &$form_state) {
  $form = array();

  module_load_include('inc', 'field_collection', 'field_collection.pages');
  // Create an empty field collection item
  $field_collection_item = entity_create('field_collection_item', array('field_name' => 'field_advance_skype_connections'));
  $entity_form = field_collection_item_form($form, $form_state, $field_collection_item);
  $form['add_skype_contacts'] = $entity_form;
  $form['add_skype_contacts']['#weight'] = '1';

  $form['action'] = array(
    '#type' => 'radios',
    '#weight' => '-1',
    '#title' => t('What action you want to perform when clicked on status icon?'),
    '#default_value' => 'chat',
    '#options' => array('chat' => t('Chat'), 'call' => t('Call')),
  );

  $form['label'] = array(
    '#markup' => t('<h1>Add new skype contact</h1>'),
    '#weight' => '0',
  );

  return $form;
}

/**
 * Function for handling form submisson.
 */
function advance_skype_configure_skype_id_submit($form, $form_state) {
  module_load_include('inc', 'field_collection', 'field_collection.pages');
  $user = user_load($GLOBALS['user']->uid);

  $skype_alias = $form_state['values']['field_advance_skype_alias']['und'][0]['value'];
  $skype_id = $form_state['values']['field_advance_skype_id']['und'][0]['value'];

  $field_collection_item = $form_state['field_collection_item'];
  $field_collection_item->setHostEntity('user', $user);
  $wrapper = entity_metadata_wrapper('field_collection_item', $field_collection_item);
  $wrapper->field_advance_skype_alias->set($skype_alias);
  $wrapper->field_advance_skype_id->set($skype_id);
  $field_collection_item->save();

  $settings = array(
    'uid' => $GLOBALS['user']->uid,
    'action' => $form_state['values']['action'],
  );
  variable_set('advance_skype_user_settings', $settings);

  drupal_set_message(t('Your settings has been saved.'));
}
