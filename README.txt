
-- SUMMARY --
  Advance Skype allows user to add their skype contacts on drupal site and see
  their Skype online status. Provided the contact has shared their skype status on web
  from their Skype accounts. It also allows user to set the default action(Chat/Call)
  to be performed when clicked on status icon.

-- INSTALLATION --
 
  1. Place the entire directory under your Drupal installation
     directory: sites/all/modules/

  2. Go to Administration » Modules and enable the Advance Skype module.

  3. Go to Administration » Structure » Blocks and add Advance Skype block to the desired region.

-- CONFIGURATION AND FEATURES --
 
  1. Go to Administration » People » Permissions for configuring permissions.

  2. You can set the count of how many Skype contacts user can add under:
     Administration » Configuration » People » Account settings » Manage fields
     edit Advance Skype Connections field. Under ADVANCE SKYPE CONNECTIONS FIELD SETTINGS
     set Number of values.

NOTE: To see your Skype Contact's current status, your contact must share their Skype status on web
      under privacy settings. For more information please visit:
      https://support.skype.com/en/faq/FA605/how-do-i-set-up-the-skype-button-to-show-my-status-on-the-web-in-skype-for-windows-desktop
